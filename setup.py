from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in stock_standard_costing/__init__.py
from stock_standard_costing import __version__ as version

setup(
	name="stock_standard_costing",
	version=version,
	description="implementation of erpnext stock standatd costing",
	author="Oscar",
	author_email="oscar@schemetical.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
