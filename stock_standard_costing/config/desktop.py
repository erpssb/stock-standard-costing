from frappe import _

def get_data():
	return [
		{
			"module_name": "Stock Standard Costing",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Stock Standard Costing")
		}
	]
