"""
Configuration for docs
"""

# source_link = "https://github.com/[org_name]/stock_standard_costing"
# docs_base_url = "https://[org_name].github.io/stock_standard_costing"
# headline = "App that does everything"
# sub_heading = "Yes, you got that right the first time, everything"

def get_context(context):
	context.brand_html = "Stock Standard Costing"
